package com.techu.apitechuv2.services;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    public List<UserModel> getUsers(String orderBy) {
        System.out.println("getUsers");
        List<UserModel> result;
        if (orderBy != null) {
            System.out.println("Se ha pedido ordenación");
            result = this.userRepository.findAll(Sort.by("age"));
        } else {
            result = this.userRepository.findAll();
        }
        return result;
    }
    public Optional<UserModel> findById(String id) {
        System.out.println("findById");
        System.out.println("Buscando usuario con id " + id);
        return this.userRepository.findById(id);
    }
    public UserModel addUser(UserModel user) {
        System.out.println("addUser");
        return this.userRepository.save(user);
    }
    public UserModel updateUser(UserModel user) {
        System.out.println("updateUser");
        return this.userRepository.save(user);
    }
    public boolean deleteUser(String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);
        boolean result = false;
        if (this.userRepository.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado, borrando");
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}