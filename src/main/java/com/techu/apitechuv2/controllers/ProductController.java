package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.Apitechuv2Application;
import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;
    static final String APIBaseURL = "/apitechu/v2";

    @GetMapping("/products")
    public List<ProductModel> getProducts() {
        System.out.println("getProducts");

        return this.productService.findAll();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {

        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        // Primera opción para devolver el resultado
        //    if (result.isPresent() == true) {
        //        return new ResponseEntity<>(result.get(), HttpStatus.OK);
        //       }
        //    else {
        //        return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del nuevo producto es " + product.getId());
        System.out.println("La descripción del nuevo producto es " + product.getDesc());
        System.out.println("El precio del nuevo producto es " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {

        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar  es " + product.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if(productToUpdate.isPresent() == true) {
            System.out.println("Producto a actualizar encontrado, actualizando...");
            this.productService.update(product);
        }
        return new ResponseEntity<>(product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteproduct(@PathVariable String id) {

        System.out.println("deleteProduct");
        System.out.println("La id del producto a actualizar es " + id);

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "producto borrado" : "Producto no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}