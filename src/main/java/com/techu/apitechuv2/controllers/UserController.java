package com.techu.apitechuv2.controllers;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping("/apitechu/v2")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderby", required = false) String orderBy
    ) {
        System.out.println("getUsers");
        System.out.println("El valor de $orderby es " + orderBy);
        return new ResponseEntity<>(
                this.userService.getUsers(orderBy),
                HttpStatus.OK
        );
    }
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserByid(@PathVariable String id) {
        System.out.println("getUserByid");
        System.out.println("La id del usuario a buscar es " + id);
        Optional<UserModel> result = this.userService.findById(id);
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());
        return new ResponseEntity<>(
                this.userService.addUser(user),
                HttpStatus.CREATED
        );
    }
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("La id del usuario que se va a actualizar en parámetro URL es " + id);
        System.out.println("La id del usuario que se va a actualizar es " + user.getId());
        System.out.println("El nombre del usuario que se va a actualizar es " + user.getName());
        System.out.println("La edad del usuario que se va a actualizar es " + user.getAge());
        Optional<UserModel> userToUpdate = this.userService.findById(id);
        if (userToUpdate.isPresent() == true) {
            System.out.println("Usuario para actualizar encontrado, actualizando");
            this.userService.updateUser(user);
        }
        return new ResponseEntity<>(user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);
        boolean deletedUser = this.userService.deleteUser(id);
        return new ResponseEntity<>(
                deletedUser ? "Usuario borrado" : "Usuario no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}