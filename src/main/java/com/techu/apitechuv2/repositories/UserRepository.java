package com.techu.apitechuv2.repositories;

import com.techu.apitechuv2.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
public interface UserRepository extends MongoRepository<UserModel, String> {

}
