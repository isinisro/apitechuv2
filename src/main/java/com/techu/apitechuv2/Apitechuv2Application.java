package com.techu.apitechuv2;

import com.techu.apitechuv2.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class Apitechuv2Application {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(Apitechuv2Application.class, args);
	}

}
